package main

import (
	"hash"
	"hash/crc32"
	"io"
)

// @ref https://stackoverflow.com/a/64419012

func NewCRCwriter(poly uint32, w io.Writer) *CRCwriter {
	return &CRCwriter{
		h: crc32.New(crc32.MakeTable(poly)),
		w: w,
	}
}

type CRCwriter struct {
	h hash.Hash32
	w io.Writer
}

func (c *CRCwriter) Write(p []byte) (n int, err error) {
	n, err = c.w.Write(p) // with each write ...
	c.h.Write(p)          // ... update the hash
	return
}

func (c *CRCwriter) Sum() uint32 { return c.h.Sum32() } // final hash
