# loadtup-dl

A tool to download subtitled videos from the website loadtup.com.

It downloads videos using `youtube-dl`; parses and converts loadtup's custom subtitle format to srt; and remuxes them together using `mkvmerge`, including the CRC32 in the resulting filename.

## Installation

```
git clone https://git.ivysaur.me/code.ivysaur.me/loadtup-dl.git
cd loadtup-dl
go build
sudo cp ./loadtup-dl /usr/local/bin/loadtup-dl
```

## Usage

```
Usage: loadtup-dl [options] [--] URL|- [URL...]

Supported URLs take the form 'https://loadtup.com/abcdefghijk'. Use a hyphen to
read equivalent loadtup.com HTML content from stdin.

Options:
  --youtube-dl PATH          Override path to youtube-dl
  --mkvmerge PATH            Override path to mkvmerge
  --mediainfo PATH           Override path to mediainfo
  --output PATH              Override output filename
                             (only valid for a single URL)
  --delete-temporary=false   Preserve temporary files
  --loglevel 0|1|2           Set verbosity (0=silent, 1=normal, 2=verbose)
```

## Changelog

v1.1.0 (2020-04-12)
- Feature: Support translator notes (`scrcaps`)
- Feature: Set stream language and title for generated mkv file
- Enhancement: Add custom logging levels
- Fix invalid characters appearing in generated filenames
- Fix misdetection of translator note usage
- Fix duration of final subtitle entry (adds dependency on `mediainfo`)

v1.0.0 (2020-04-11)
- Initial public release
